import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Navbar from "./Navbar";
import "./css/reserva.css";

const Reserva = () => {

  //use state
  const [separa, separacion] = useState({
    id_laboratorio: "",
    id_docente: "",
    id_materia: "",
    numero_maquinas_usar: "",
    fecha_reserva: "",
    hora_inicio: "",
    hora_fin: "",
  });
  const [editing, setEditing] = useState (false);
  const [loading, setLoading] = useState (false);
  const params = useParams ();
  const navigate = useNavigate();


  //funcion de captura de datos
  const handleSubmit = async (e) => {
    e.preventDefault();
    setErrores(validateForm(separa));
    
    setLoading(true);
    if (editing){
      await fetch(`http://localhost:4000/reserva/${params.id_reservar_laboratorio}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(separa),
      })
    } else if (Object.keys(errores).length === 0){
      
     await fetch("http://localhost:4000/reserva", {
        method: "POST",
        body: JSON.stringify(separa),
        headers: { "Content-Type": "application/json" },
      });
      
  
      // if (data === "reserva no guardada") {
      //   alert("error al hacer la reserva");
      // } else if (data === "reserva guardada") {
      //   alert("reserva realizada con exito");
      //   navigate("/reserva");
      // }
    }else{
      return;
    }
    setLoading(false);
    if (editing){
      alert('Se ha editado la reserva exitosamente')
    } else {
      alert('Se ha reservado el laboratorio exitosamente')
    }
    navigate("/reserva")
  };

  const [labs, setLabs] = useState([]);
  const loadLab = async () => {
    const response = await fetch("http://localhost:4000/lab");
    const data = await response.json();
    setLabs(data);
  };


  useEffect(() => {
    loadLab();
  }, []);


  const [materias, setMateria] = useState([]);
  const loadMateria = async () => {
    const response = await fetch("http://localhost:4000/materia");
    const data = await response.json();
    setMateria(data);
  };


  useEffect(() => {
    loadMateria();
  }, []);


  const [docentes, setDocentes] = useState([]);
  const loadDocente = async () => {
    const response = await fetch("http://localhost:4000/docente");
    const data = await response.json();
    setDocentes(data);
  };


  useEffect(() => {
    loadDocente();
  }, []);



  const handleChange = (e) => {
    separacion({ ...separa, [e.target.name]: e.target.value });
  };

  const loadReserva = async (id_reservar_laboratorio) => {
    const res = await fetch (`http://localhost:4000/reserva/${id_reservar_laboratorio}`);
    const data = await res.json();
    separacion({
      id_laboratorio: data.id_laboratorio,
      id_docente: data.id_docente,
      id_materia: data.id_materia,
      numero_maquinas_usar: data.numero_maquinas_usar,
      fecha_reserva: data.fecha_reserva,
      hora_inicio: data.hora_inicio,
      hora_fin: data.hora_fin,
    });
    setEditing(true);
  };

  useEffect(() => {
    if (params.id_reservar_laboratorio){
      loadReserva(params.id_reservar_laboratorio);
    }
  },[params.id_reservar_laboratorio])

  const validateForm = (separa) => {
    const tiempoTranscurrido = Date.now();
    const hoy = new Date(tiempoTranscurrido);
    let errores = {};
    

    if (separa.numero_maquinas_usar === "") {
        errores.numero_maquinas_usar = "el campo es requerido"
    } else if (parseInt(separa.numero_maquinas_usar, 10) <= 9) {
        errores.numero_maquinas_usar = "debes separar al menos 10 máquinas"
    } else if (parseInt(separa.numero_maquinas_usar, 10) >= 30) {
        errores.numero_maquinas_usar = "no pueden ser mas de 29 maquinas"
    }

    const hoy2 = new Date(separa.fecha_reserva);
    if (separa.fecha_reserva === "") {
        errores.fecha_reserva = "la fecha no puede estar vacia"
    } else if ((Date.parse(hoy2.toLocaleDateString()) + 3008400000) < Date.parse(hoy.toLocaleDateString())) {
        errores.fecha_reserva = "no puedes reservar en el pasado"
    } 
    // else if(Date.parse(separa.fecha_reserva)  < hoy.toLocaleDateString()){
    //     errores.fecha_reserva = "fecha invalida"
    // }

    if (separa.hora_inicio === "") {
        errores.hora_inicio = "la hora de inicio no puede estar vacia"
    }

    const rango = (inicio, fin) => {
        var minutos_inicio = inicio.split(':')
            .reduce((p, c) => parseInt(p) * 60 + parseInt(c));
        var minutos_final = fin.split(':')
            .reduce((p, c) => parseInt(p) * 60 + parseInt(c));

        var diferencia = minutos_final - minutos_inicio;
        return diferencia;
    }
    if (separa.hora_fin === "") {
        errores.hora_fin = "la hora de fin no puede estar vacia"
    } else if (separa.hora_fin < separa.hora_inicio) {
        errores.hora_fin = "la hora de fin no puede ser menor a la hora de inicio"
    } else if (rango(separa.hora_inicio, separa.hora_fin) < 30) {
        errores.hora_fin = "al menos 30 min de reserva"
    } else if (rango(separa.hora_inicio, separa.hora_fin) > 60) {
        errores.hora_fin = "la reserva no puede exeder la hora"
    }

    if (separa.id_docente === "") {
        errores.id_docente = "debe seleccionar un docente"

    }
    if (separa.id_materia === "") {
        errores.id_materia = "debe seleccionar una materia"
    }
    if (separa.id_laboratorio === "") {
        errores.id_laboratorio = "debe seleccionar un laboratorio"
    }
    return errores;
};
const [errores, setErrores] = useState({});
const handleBlur = (e) => {
  handleChange(e);
  setErrores(validateForm(separa))
};
  return (
    <>
      <Navbar/>
      <div className="container-reserva">
        <h1>{editing ? "Editar Reserva" : "Reservar Laboratorio"}</h1>
        <form className="reserva" onSubmit={handleSubmit}>
          <div className="date">
            <select
              className="select-reserva"
              name="id_laboratorio"
              id="Elab"
              onChange={handleChange}
              defaultValue={'DEFAULT'} 
              onBlur={handleBlur}
            >
              <option disabled value="DEFAULT" >Selecciona un laboratorio</option>
              {labs.map((laboratorio) => (
                <option
                  key={laboratorio.id_laboratorio}
                  value={laboratorio.id_laboratorio}
                >
                  {laboratorio.nombre_laboratorio}
                </option>
              ))}
            </select>
            <spam></spam>
            <label className="name-label">Laboratorio</label>
          </div>
          {errores.id_laboratorio && <p className='error'>{errores.id_laboratorio}</p>}

          <div className="date">
            <select
              name="id_docente"
              className="tamaño"
              onChange={handleChange}
              defaultValue={'DEFAULT'} 
              onBlur={handleBlur}
            >
              <option disabled value="DEFAULT" >Selecciona un Docente</option>
              {docentes.map((docente) => (
                <option key={docente.id_docente} value={docente.id_docente}>
                  {docente.nombre_docente + " " + docente.apellido_docente}
                </option>
              ))}
            </select>
            <spam></spam>
            <label>Docente</label>
          </div>
          {errores.id_docente && <p className='error'>{errores.id_docente}</p>}

          <div className="date">
            <select
              name="id_materia"
              className="tamaño"
              onChange={handleChange}
              defaultValue={'DEFAULT'} 
              onBlur={handleBlur}
            >
              <option disabled value="DEFAULT" >Selecciona una materia</option>
              {materias.map((materia) => (
                <option key={materia.id_materia} value={materia.id_materia}>
                  {materia.nombre_materia}
                </option>
              ))}
            </select>
            <spam></spam>
            <label>Materia</label>
          </div>
          {errores.id_materia && <p className='error'>{errores.id_materia}</p>}

          <div className="date">
            <input
              type="number"
              required
              name="numero_maquinas_usar"
              min="1"
              max="40"
              onChange={handleChange}
              onBlur={handleBlur}
            />
            <spam></spam>
            <label>Numero de maquinas a usar</label>
          </div>
          {errores.numero_maquinas_usar && <p className='error'>{errores.numero_maquinas_usar}</p>}

          <div className="date">
            <input type="date" name="fecha_reserva" onChange={handleChange} onBlur={handleBlur}/>
            <spam></spam>
            <label>fecha de reserva</label>
          </div>
          {errores.fecha_reserva && <p className='error'>{errores.fecha_reserva}</p>}

          <div className="date">
            <input type="time" name="hora_inicio" onChange={handleChange} onBlur={handleBlur}/>
            <spam></spam>
            <label>Hora inicio</label>
          </div>
          {errores.hora_inicio && <p className='error'>{errores.hora_inicio}</p>}

          <div className="date">
            <input type="time" name="hora_fin" onChange={handleChange} onBlur={handleBlur}/>
            <spam></spam>
            <label>Hora fin</label>
          </div>
          {errores.hora_fin && <p className='error'>{errores.hora_fin}</p>}
          <input
            className="btn-guardar-reserva"
            type="submit"
            value={editing ? "Actualizar" : "Guardar"}
          />
        </form>
      </div>
    </>
  );
};

export default Reserva;
