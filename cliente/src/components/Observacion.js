import React from "react";
import "./css/observaciones.css";
import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Navbar from "./Navbar";

const Observacion = () => {

  const [observacion, setObservacion] = useState({
    id_docente: "",
    descripcion_recomedacion: "",
    descripcion_observacion: ""
  });
  const [docentes, setDocentes] = useState([]);
  const [loading, setLoading] = useState(false);
  const [editing, setEditing] = useState(false);

  const navigate = useNavigate();
  const params = useParams();

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    setErrores(validateForm(observacion));
    if (editing) {
      await fetch(`http://localhost/observacion/${params.id_observacion}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(observacion)
      });
    } else if (Object.keys(errores).length === 0){
      await fetch("http://localhost:4000/observacion", {
        method: "POST",
        body: JSON.stringify(observacion),
        headers: { "Content-Type": "application/json" },
      });
    }
    setLoading(false);
    if (editing) {
      alert("Observacion editada exitosamente")
      navigate("/observacion");
    } else if(Object.keys(errores).length === 0){
      alert("Observacion enviada exitosamente")
      navigate("/observacion");
    }else{
      alert("no se pudo guardar la observacion")
    }
    

  };


  const handleChange = (e) => {
    setObservacion({ ...observacion, [e.target.name]: e.target.value });
  };
  const loadDocente = async () => {
    const response = await fetch("http://localhost:4000/docente");
    const data = await response.json();
    setDocentes(data);
  };
  useEffect(() => {
    loadDocente();
  }, []);


  const [errores, setErrores] = useState({});
  const validateForm = (observacion) => {

    let errores = {};

    let observ = /^[a-zA-ZÀ-ÿ\s]{20,255}$/; // Letras y espacios, pueden llevar acentos.



    if (observacion.descripcion_observacion === "") {
      errores.descripcion_observacion = "requiere completar el campo"
    } else if (observ.test(observacion.descripcion_observacion) === false) {
      errores.descripcion_observacion = "solo letras y espacios, pueden llevar acentos"
    }

    if (observacion.id_docente === "" || observacion.id_docente === "DEFAULT") {
      errores.id_docente = "requiere completar el campo"
    }

    if (observacion.descripcion_recomedacion !== "") {
      if(observ.test(observacion.descripcion_recomedacion)=== false){
        errores.descripcion_recomedacion = "solo letras y espacios, pueden llevar acentos"
      }
    } 

    return errores;
  };



  const handleBlur = (e) => {
    handleChange(e);
    setErrores(validateForm(observacion))
  };

  return (
    <>
      <Navbar />
      <div className="contenedora_historial">
        <section className="seccion_historial">
          <h1 className="titulo" style={{ textAlign: "center" }}>
            OBSERVACIONES DE LABORATORIOS
          </h1>
          <form onSubmit={handleSubmit}>
            <div className="contenedor_obs">
              <div className="date-observacion">
                <label>Docente:</label>
                <select className="docente-select" name="id_docente" onChange={handleChange} onBlur={handleBlur} defaultValue={'DEFAULT'} >
                  <option disabled value="DEFAULT" >Selecciona una materia</option>
                  {docentes.map((docente) => (
                    <option key={docente.id_docente} value={docente.id_docente}>
                      {docente.nombre_docente + " " + docente.apellido_docente}
                    </option>
                  ))}
                </select>
                
                {errores.id_docente && <p className='error'>{errores.id_docente}</p>}
              </div>


              <div className="texto_obs1">
                <label>Observaciones:</label>
                <textarea
                  name="descripcion_observacion"
                  rows={5}
                  cols={40}
                  id="obser"
                  defaultValue={""}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {errores.descripcion_observacion && <p className='error'>{errores.descripcion_observacion}</p>}
              </div>

              <div className="texto_obs2">
                <label>Recomendaciones:</label>
                <textarea
                  name="descripcion_recomedacion"
                  rows={5}
                  cols={40}
                  id="recom"
                  defaultValue={""}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                {errores.descripcion_recomedacion && <p className='error'>{errores.descripcion_recomedacion}</p>}

              </div>

              <div className="boton">

                {/* <button className="botn_obs" id="boton" onclick="validar()">Enviar comentarios</button> */}
                <input
                  type="submit"
                  value="Enviar Observación"
                  className="botn_obs"
                />
              </div>
            </div>
          </form>
        </section>
      </div>
    </>
  );
};

export default Observacion;
