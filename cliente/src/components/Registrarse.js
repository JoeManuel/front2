import { React, useState, useEffect } from "react";
import "./css/registrarse.css";
import { useNavigate } from "react-router-dom";
// import { parse } from "pg-protocol";
// import Navbar from "./Navbar";

const Registrarse = () => {
  //useState
  const [docente, setDocente] = useState({
    nombre_docente: "",
    apellido_docente: "",
    correo_docente: "",
    telefono_docente: "",
    nombre_usuario: "",
    contrasena_usuario: "",
    repetir_contrasena: "",
    usuario_admin: "false"
  });

  //estado de cargando
  const [loading, setLoading] = useState(false);

  //definiendo el usenavigate
  const navigate = useNavigate();

  //evento de captura de datos
  const handleSubmit = async (e) => {
    e.preventDefault();

    //establecemos el loading
    setLoading(true);
    setErrores(validateForm(docente));
    if (Object.keys(errores).length === 0){

      const res = await fetch("http://localhost:4000/docente", {
      method: "POST",
      body: JSON.stringify(docente),
      headers: { "Content-Type": "application/json" },
    });
    const data = await res.json();
    console.log(data);
    alert("datos registrados con exito")
    setLoading(false);
    navigate("/");
    }else{

      return;
    }
    
  };

  const handleChange = (e) => {
    setDocente({ ...docente, [e.target.name]: e.target.value });
  };
  const [errores, setErrores] = useState({});
  const validateForm = (docente) => {

    let errores = {};
    
     let usuario= /^[a0-9]{11}$/; // Letras, numeros, guion y guion_bajo
     let nombre= /^[a-zA-ZÀ-ÿ\s]{1,40}$/; // Letras y espacios, pueden llevar acentos.
     let password= /^.{4,30}$/; // 4 a 30 digitos.
     let correo= /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
     let telefono= /^\d{7,10}$/; // 7 a 14 numeros.
    

    if (docente.nombre_docente === "") {
      errores.nombre_docente = "requiere completar el campo"
    }else if(nombre.test(docente.nombre_docente) === false){
      errores.nombre_docente = "solo letras| no mas de 40 caracteres"
    }

    if (docente.apellido_docente === "") {
      errores.apellido_docente = "requiere completar el campo"
    }else if(nombre.test(docente.apellido_docente) === false){
      errores.apellido_docente = "solo letras| no mas de 40 caracteres"
    }

    if (docente.correo_docente === "") {
      errores.correo_docente  = "requiere completar el campo"
    }else if(correo.test(docente.correo_docente ) === false){
      errores.correo_docente  = "correo invalido, intente denuevo"
    }
    if (docente.telefono_docente === "") {
      errores.telefono_docente  = "requiere completar el campo"
    }else if(telefono.test(docente.telefono_docente ) === false){
      errores.telefono_docente  = "solo números, no menor a 7 o mayor a 10 digitos"
    }

    if (docente.nombre_usuario === "") {
      errores.nombre_usuario  = "requiere completar el campo"
    }else if(usuario.test(docente.nombre_usuario ) === false){
      errores.nombre_usuario  = "la primera letra 'a' y el resto es la cedula 'a1321562890'"
    }

    if (docente.contrasena_usuario === "") {
      errores.contrasena_usuario = "requiere completar el campo"
    }else if(password.test(docente.contrasena_usuario ) === false){
      errores.contrasena_usuario  = "no debe ser menor a 4, o mayor a 30 caracteres'"
    }
    if (docente.repetir_contrasena === "") {
      errores.repetir_contrasena = "requiere completar campo"
    }else if(docente.contrasena_usuario  !== docente.repetir_contrasena){
      errores.repetir_contrasena  = "contraseñas no coinciden"
    }
    

    return errores;
  };

  

  const handleBlur = (e) => {
    handleChange(e);
    setErrores(validateForm(docente))
  };
  return (
    // <div>
    /* <Navbar /> */
    <div className="container-registrarse">
      <h1> Registrate:</h1>
      <form className="registrarse" onSubmit={handleSubmit}>
        <div className="date">
        {errores.nombre_docente && <p className='error'>{errores.nombre_docente}</p>}
          <input
            type="text"
            required
            onChange={handleChange}
            onBlur={handleBlur}
            name="nombre_docente"
          />
          <span></span>
          <label>Nombres</label>
          
        </div>
        
        <div className="date">
        {errores.apellido_docente && <p className='error'>{errores.apellido_docente}</p>}
          <input
            type="text"
            required
            onChange={handleChange}
            onBlur={handleBlur}
            name="apellido_docente"
          />
          <span></span>
          <label>Apellidos</label>
        </div>
        <div className="date">
        {errores.correo_docente && <p className='error'>{errores.correo_docente}</p>}
          <input
            type="text"
            required
            onChange={handleChange}
            onBlur={handleBlur}
            name="correo_docente"
          />
          <span></span>
          <label>Correo electrónico</label>
        </div>
        <div className="date">
        {errores.telefono_docente && <p className='error'>{errores.telefono_docente}</p>}
          <input
            type="text"
            required
            onChange={handleChange}
            onBlur={handleBlur}
            name="telefono_docente"
          />
          <span></span>
          <label>Teléfono</label>
        </div>
        <div className="date">
        {errores.nombre_usuario && <p className='error'>{errores.nombre_usuario}</p>}
          <input
            type="text"
            required
            onChange={handleChange}
            onBlur={handleBlur}
            name="nombre_usuario"
          />
          <span></span>
          <label>Nombre de Usuario</label>
        </div>
        <div className="date">
        {errores.contrasena_usuario && <p className='error'>{errores.contrasena_usuario}</p>}
          <input
            type="password"
            required
            onChange={handleChange}
            onBlur={handleBlur}
            name="contrasena_usuario"
          />
          <span></span>
          <label>Contraseña</label>
        </div>
        <div className="date">
        {errores.repetir_contrasena && <p className='error'>{errores.repetir_contrasena}</p>}
          <input
            type="password"
            required
            onChange={handleChange}
            onBlur={handleBlur}
            name="repetir_contrasena"
          />
          <span></span>
          <label>Repetir Contraseña</label>
        </div>
        <input className="btn-login" type="submit" value="Registrar" />
        <div className="link-registrarse">
          Ya estas registrado?{" "}
          <a className="enlace-iniciar sesion" href="/">
            {" "}
            Inicar sesión
          </a>
        </div>
      </form>
    </div>
    // </div>
  );
};

export default Registrarse;
